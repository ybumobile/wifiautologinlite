package com.ybu.wifiautologin;

public interface Constants {
	final int HIST_LEN = 30;

	final String PREFS_NAME = "wifiautologin";
	final String PREF_KEY_ACTIVE = "active";
	final String PREF_KEY_REFRESH = "refresh";
	final String PREF_KEY_FORCE = "force";

	final String PREF_KEY_NOTIFY_WHEN_SUCCESS = "NOTIFY_WHEN_SUCCESS";
	final String PREF_KEY_NOTIFY_WHEN_ERROR = "NOTIFY_WHEN_ERROR";
	final String PREF_KEY_NOTIFY_WHEN_ALREADY_LOGGED_IN = "NOTIFY_WHEN_ALREADY_LOGGED_IN";

	final String ATTWIFI_SSID = "attwifi";
	final String WAYPORT_SSID = "Wayport_Access";
	// final String CANSTARBUCKS_SSID = "hotspot_Bell";
	final String QWEST_SSID = "QwestWiFi";

	final int HOTSPOT_METHOD_1 = 1;
	
	final String METHOD_CONFIG = "formPattern:<form(.*?)>,"
			+ "inputPattern:<input(.*?)>,"
			+ "attributePattern:([\\w:\\-]+)(=(\"(.*?)\"|'(.*?)'|([^ ]*))|(\\s+|\\z))";
	
	final long REFRESH_INTERVAL_MS = 1000;

}
