package com.ybu.wifiautologin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.ybu.wifiautologin.db.DBAccesser;
import com.ybu.wifiautologin.model.HistoryItem;
import com.ybu.wifiautologin.model.LoginMethodItem;

public class NetStatusBroadcastReceiver extends BroadcastReceiver {
	private static final String TAG = "WifiAutoLogin";
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;

		final String action = intent.getAction();
		Log.d(TAG, "Broadcast received. Action=" + action);

		SharedPreferences settings = context.getSharedPreferences(
				Constants.PREFS_NAME, 0);
		if (!settings.getBoolean(Constants.PREF_KEY_ACTIVE, true)) {
			Log.i(TAG, "Disabled. Ignoring broadcast.");
			return;
		}

		// check if device is connected to any network
		NetworkInfo ni = (NetworkInfo) intent
				.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
		if (ni == null || !ni.isConnected()) {
			Log.d(TAG, "Not connected");
			return;
		}

		// check the current connected wifi network
		WifiManager wifi = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);

		WifiInfo winfo = wifi.getConnectionInfo();
		String ssid = winfo.getSSID();
		DBAccesser db = new DBAccesser(context);

		LoginMethodItem loginMethod = getLoginMethod(db, ssid);
		if (loginMethod == null) {
			Log.d(TAG, "Unknown SSID " + ssid);
			return;
		}

		Hotspot s = getLoginClass(loginMethod);
		if (s == null) {
			Log.d(TAG, "Unknown login method id " + loginMethod.getMethodId());
			return;
		}

		SharedPreferences prefs = context.getSharedPreferences(
				Constants.PREFS_NAME, Context.MODE_PRIVATE);
		HistoryItem h = new HistoryItem();
		h.setDate(new Date());
		Log.d(TAG, "Hotspot SSI detected. SSID = " + ssid);

		h.setMessage("Trying to log in to " + ssid);
		try {
			boolean status = s.login(context, ssid);
			h.setSuccess(true);
			if (status) {
				if (prefs.getBoolean(Constants.PREF_KEY_NOTIFY_WHEN_SUCCESS,
						true)) {
					createNotification(context
							.getString(R.string.notify_message_success));
				}
				h.setMessage("Connected to Internet.");
			} else {
				if (prefs
						.getBoolean(
								Constants.PREF_KEY_NOTIFY_WHEN_ALREADY_LOGGED_IN,
								false)) {
					createNotification(context
							.getString(R.string.notify_message_already_logged));
				}
				h.setMessage("Connected to Internet.");
			}
		} catch (Exception e) {
			URL testURL;
			try {
				testURL = new URL("http://www.wifiautologin.com/ping/");

				HttpURLConnection conn = (HttpURLConnection) testURL
						.openConnection();
				StringBuilder page = getHttpContent(conn);
				boolean isRealPage = page.toString().contains("pong");
				if (!isRealPage) {
					createNotification(context
							.getString(R.string.notify_message_error));

					Log.e(TAG, "Login failed", e);
					h.setSuccess(false);
					h.setMessage("Login to " + ssid + " failed: "
							+ e.getMessage());

				} else {
					createNotification(context
							.getString(R.string.notify_message_success));

					h.setSuccess(true);
					h.setMessage("Connected to Internet.");
				}
				db.addHistoryItem(h);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		db.addHistoryItem(h);
		// add separator
		h.setMessage("sep");
		db.addHistoryItem(h);
	}

	private LoginMethodItem getLoginMethod(DBAccesser db, String ssid) {
		Map<String, LoginMethodItem> loginMethods = db.getLoginMethods();
		Collection<String> supportedSsids = new HashSet<String>();
		supportedSsids.addAll(loginMethods.keySet());
		supportedSsids.retainAll(Hotspot.getSupportedSsids());

		return supportedSsids.contains(ssid) ? loginMethods.get(ssid) : null;
	}

	private Hotspot getLoginClass(LoginMethodItem item) {
		if (item.getMethodId() == Constants.HOTSPOT_METHOD_1) {
			return new Hotspot(item.getMethodConfig());
		} else {
			return null;
		}
	}

	private void createNotification(String message) {
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(
				R.drawable.wifiautologin32,
				context.getString(R.string.notify_title),
				System.currentTimeMillis());
		Intent notificationIntent = new Intent(context, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notification.setLatestEventInfo(context,
				context.getString(R.string.notify_title), message,
				contentIntent);
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}

	// Mark's method to retrieve html content, need to be put somewhere else
	private StringBuilder getHttpContent(HttpURLConnection conn)
			throws IOException {
		// get the HTML of the webpage
		BufferedReader in = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String line;
		StringBuilder html = new StringBuilder();
		while ((line = in.readLine()) != null) {
			html.append(line);
		}
		in.close();
		return html;
	}
}
