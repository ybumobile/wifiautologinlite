package com.ybu.wifiautologin.db;

import java.io.File;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * @author Igor Giziy <linsalion@gmail.com>
 */
public class DBCreator extends SQLiteOpenHelper {
	private static final String TAG = "WiFiAutoLogin";
	private static final String DATABASE_NAME = "wifiautologin.db";
	private static final int DATABASE_VERSION = 3;
	private Context context;
	private DBAccesser accessor;
	
	private static final String CREATE_HISTORY_TABLE = "create table history "
			+ " (_id integer primary key autoincrement, " + "date integer, "
			+ "success integer, " + "message text);";
	private static final String CREATE_SSID_TABLE = "create table ssids "
			+ " (_id integer primary key autoincrement, "
			+ "ssid varchar(40), " + "host varchar(255), "
			+ "description text);";
	
	// method_id: A hard coded integer tells the program which java class to use.
	// method_config: A string containing details login method configurations.
	// Such as HTML form pattern, input pattern and attribute pattern.
	private static final String CREATE_LOGIN_METHOD_TABLE = "create table login_methods "
			+ " (_id integer primary key autoincrement, "
			+ "ssid varchar(40), "
			+ "method_id integer, "
			+ "method_config text);";	
	
	public DBCreator(DBAccesser accessor, Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		this.accessor = accessor;
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		Log.d(TAG, "Creating table:" + CREATE_LOGIN_METHOD_TABLE);
		sqLiteDatabase.execSQL(CREATE_LOGIN_METHOD_TABLE);
		Log.d(TAG, "Creating table:" + CREATE_HISTORY_TABLE);
		sqLiteDatabase.execSQL(CREATE_HISTORY_TABLE);
		
		accessor.invalidateData();
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
		sqLiteDatabase.execSQL("drop table if exists history");
		sqLiteDatabase.execSQL("drop table if exists login_methods");
		onCreate(sqLiteDatabase);
	}
	
	public synchronized SQLiteDatabase getWritableDatabase() {
		SQLiteDatabase db;
		try {
			db = super.getWritableDatabase();
		} catch (SQLiteException e) {
			Log.e(TAG, e.getMessage());
			File dbFile = context.getDatabasePath(DATABASE_NAME);
			db = SQLiteDatabase.openDatabase(dbFile.getAbsolutePath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		}
		return db;
	}

	public synchronized SQLiteDatabase getReadableDatabase() {
		SQLiteDatabase db;
		try {
			db = super.getReadableDatabase();
		} catch (SQLiteException e) {
			Log.e(TAG, e.getMessage());
			File dbFile = context.getDatabasePath(DATABASE_NAME);
			db = SQLiteDatabase.openDatabase(dbFile.getAbsolutePath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		}
		return db;
	}

}
