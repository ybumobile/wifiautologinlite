package com.ybu.wifiautologin.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ybu.wifiautologin.Constants;
import com.ybu.wifiautologin.model.HistoryItem;
import com.ybu.wifiautologin.model.LoginMethodItem;

/**
 * @author Igor Giziy <linsalion@gmail.com>
 */
public class DBAccesser {
	private static final String[] METHOD_1_SSIDS = { Constants.ATTWIFI_SSID,
			Constants.QWEST_SSID, Constants.WAYPORT_SSID, "snowy" /* For testing */};
	
	private static final String TAG = "WifiAutoLogin";
	private SQLiteDatabase db;
	private DBCreator dbCreator;
	// Need to refresh this when a new version of method definition
	// file is inserted into the database.
	private static Map<String, LoginMethodItem> loginMethods = new HashMap<String, LoginMethodItem>();

	public DBAccesser(Context context) {
		dbCreator = new DBCreator(this, context);
	}

	public void invalidateData() {
		loginMethods = new HashMap<String, LoginMethodItem>();
	}

	public synchronized Map<String, LoginMethodItem> getLoginMethods() {
		if (loginMethods.isEmpty()) {
			loginMethods = readLoginMethods();
			if (loginMethods.isEmpty()) {
				initDatabase();
				loginMethods = readLoginMethods();
			}
		}
		return loginMethods;
	}

	private HashMap<String, LoginMethodItem> readLoginMethods() {
		HashMap<String, LoginMethodItem> result = new HashMap<String, LoginMethodItem>();
		try {
			db = dbCreator.getReadableDatabase();
			Cursor cursor = db.rawQuery("select * from login_methods", null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				LoginMethodItem item = readLoginMethodItem(cursor);
				Log.d(TAG, "Reading result:" + item.toString());
				result.put(item.getSsid(), item);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
			Log.d(TAG, e.toString());
		} finally {
			db.close();
		}

		return result;
	}

	private LoginMethodItem readLoginMethodItem(Cursor cursor) {
		LoginMethodItem item = new LoginMethodItem();
		item.setId(cursor.getInt(0));
		item.setSsid(cursor.getString(1));
		item.setMethodId(cursor.getInt(2));
		item.setMethodConfig(cursor.getString(3));
		return item;
	}

	private void initDatabase() {
		// The following code is used for testing using the database.
		// The idea is eventually we will populate the database table
		// login_methods
		// from a file downloaded from our webserver.
		SQLiteDatabase db = null;
		try {
			Log.d(TAG, "Initializing database...");

			db = dbCreator.getWritableDatabase();
			for (String ssid : METHOD_1_SSIDS) {
				ContentValues contentValue = new ContentValues();
				contentValue.put("ssid", ssid);
				contentValue.put("method_id", Constants.HOTSPOT_METHOD_1);
				contentValue.put("method_config", Constants.METHOD_CONFIG);

				db.insert("login_methods", null, contentValue);
			}
		} finally {
			db.close();
		}
	}

	public void addHistoryItem(HistoryItem historyItem) {
		try {
			db = dbCreator.getWritableDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put("date", historyItem.getDate().getTime());
			contentValues.put("success", historyItem.isSuccess() ? 1 : 0);
			contentValues.put("message", historyItem.getMessage());
			db.insert("history", null, contentValues);
		} finally {
			db.close();
		}
	}

	public void addHistoryItems(ArrayList<HistoryItem> historyItems) {
		for (HistoryItem historyItem : historyItems) {
			addHistoryItem(historyItem);
		}
	}

	@SuppressWarnings("unused")
	private HistoryItem getHistoryItem(int id) {
		try {
			db = dbCreator.getReadableDatabase();
			Cursor cursor = db.rawQuery("select * from history where _id = "
					+ id, null);
			try {
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					return readHistoryItem(cursor);
				} else
					return null;
			} finally {
				cursor.close();
			}
		} finally {
			db.close();
		}
	}

	private HistoryItem readHistoryItem(Cursor cursor) {
		HistoryItem historyItem = new HistoryItem();
		historyItem.setId(cursor.getInt(0));
		historyItem.setDate(new Date(cursor.getLong(1)));
		historyItem.setSuccess(cursor.getInt(2) != 0);
		historyItem.setMessage(cursor.getString(3));
		return historyItem;
	}

	public ArrayList<HistoryItem> getHistoryItems(int n) {
		ArrayList<HistoryItem> historyItems = new ArrayList<HistoryItem>();
		try {
			db = dbCreator.getReadableDatabase();
			Cursor cursor = db
					.rawQuery("select * from history order by date desc limit "
							+ n + ";", null);
			try {
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					historyItems.add(readHistoryItem(cursor));
					cursor.moveToNext();
				}
				return historyItems;
			} finally {
				cursor.close();
			}
		} finally {
			db.close();
		}
	}

	public void removeHistoryItem(int id) {
		try {
			db = dbCreator.getWritableDatabase();
			db.delete("history", "_id = " + id, null);
		} finally {
			db.close();
		}
	}

	public void removeHistoryItems() {
		try {
			db = dbCreator.getWritableDatabase();
			db.delete("history", null, null);
		} finally {
			db.close();
		}

	}

	public int getMaxId() {
		try {
			db = dbCreator.getReadableDatabase();
			Cursor cursor = db.rawQuery("SELECT max(_id) from history", null);
			try {
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					return cursor.getInt(0);
				} else {
					return 0;
				}
			} finally {
				cursor.close();
			}
		} finally {
			if (db != null)
				db.close();
		}
	}
}
